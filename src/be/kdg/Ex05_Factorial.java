package be.kdg;

public class Ex05_Factorial {
    public static void main(String[] args) {

        /* Factorial
        n! = 1⋅2⋅3⋅4⋅...⋅n
        For example:
        3! = 1⋅2⋅3 = 6
        4! = 1⋅2⋅3⋅4 = 24
        5! = 1⋅2⋅3⋅4⋅5 = 120
         */

        // the simplest approach
        System.out.println("Approach 1");
        long factor = 1;
        for (int cnt = 1; cnt <= 20; cnt++) {
            factor = factor * cnt;
            System.out.println(cnt + "! = " + factor);
        }
        System.out.println();

        System.out.println("Approach discussed during class");
        // another approach as discussed during class
        for (int i = 1; i <= 20; i++) {
            long calc = 1;
            for (int j = 1 ; j <= i; j++) {
                calc = j * calc;
            }
            System.out.println(i + "! :" + calc);
        }
    }
}
