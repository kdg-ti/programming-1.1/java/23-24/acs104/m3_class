package be.kdg;

public class Ex00_Ternary {
    public static void main(String[] args) {
        int a = 5;
        int b = 8;
        int result;

        // dirty way
        result = a - b;
        if (a < b) result = a + b;
        System.out.println("result: " + result);
        System.out.println("-----");

        // with "clear" if .. else ..
        if (a < b) {
            result = a + b;
        } else {
            result = a - b;
        }
        System.out.println("result: " + result);
        System.out.println("-----");

        // with ternary operator
        result = ((a < b) ? (a + b) : (a - b));
        System.out.println("result: " + result);
        System.out.println("-----");
    }
}
