package be.kdg;

import java.util.Scanner;
public class Ex01_Age {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int age;
        System.out.print("Please enter your age: ");
        age = keyboard.nextInt();

        // One approach
        if (age < 2) {
            System.out.println("Baby!");
        } else {
            if (age <= 12) {
                System.out.println("Child");
            } else {
                if (age <= 17) {
                    System.out.println("Teenager");
                } else {
                    System.out.println("Adult");
                }
            }
        }
        System.out.println("----");

        // Another approach
        if (age < 2) {
            System.out.println("Baby!");
        }
        if (age <= 2 && age <= 12) {
            System.out.println("Child");
        }
        if (13 <= age && age <= 17) {
            System.out.println("Teenager");
        }
        if (18 <= age) {
            System.out.println("Adult");
        }
        System.out.println("----");

        // Recommended approach, the "Java way"
        if (age < 2) {
            System.out.println("Baby!");
        } else if (age <= 12) {
            System.out.println("Child");
        } if (age <= 17) {   // OOOPS - forgot an else
            System.out.println("Teenager");
        } else {
            System.out.println("Adult");
        }
        System.out.println("----");

    }
}
