package be.kdg;

public class Ex03_LoopDescending {
    public static void main(String[] args) {
        int counter;
        int limit = 100;

        // 120 to 100 descending with while
        counter = 120;
        System.out.println("Numbers from " + counter + " to " + limit + " with while: ");
        while (counter >= limit) {
            System.out.print(counter + " ");
            counter--;
        }

        System.out.println();

        // 120 to 100 descending with do..while
        counter = 120;
        System.out.println("Numbers from " + counter + " to " + limit + " with do-while: ");
        do {
            System.out.print(counter + " ");
            counter--;
        } while(counter >= limit);

        System.out.println();

        // 120 to 100 descending with for..loop
        System.out.println("Numbers from " + 120 + " to " + limit + " with for loop: ");
        for (counter = 120; counter >= limit; counter--) {
            System.out.print(counter + " ");
        }
    }
}
